# You need to ensure the directory ~/jenkins exists:
mkdir jenkins

# Run Docker Compose

Jenkins is running in localhost:8081.

Firs Log in

View the generated administrator password to log in the first time.
❯ docker exec jenkins cat /var/jenkins_home/secrets/initialAdminPassword

# Upgrading Versions
To upgrade to latest versions just modify the tag of the image in your docker-compose yml file, for example:
/jenkins-config/docker-compose.yml

version: '3.7'
services:
  jenkins:
    image: jenkins/jenkins:2.223.1
    ...

- https://www.cloudbees.com/blog/how-to-install-and-run-jenkins-with-docker-compose


## Build From Dockerfile
docker build -t jenkins:jcasc .
docker run --name jenkins --rm -p 8080:8080 --env JENKINS_ADMIN_ID=admin --env JENKINS_ADMIN_PASSWORD=P@ssw0rd jenkins:jcasc

# Refs
- https://www.digitalocean.com/community/tutorials/how-to-automate-jenkins-setup-with-docker-and-jenkins-configuration-as-code