job('Nodejs Example CI CD') {
    scm {
        git('https://gitlab.com/nodejs136/nodejs-example-ci-cd.git') {  node -> // is hudson.plugins.git.GitSCM
            node / gitConfigName('DSL User')
            node / gitConfigEmail('jenkins-dsl@newtech.academy')
        }
    }
    triggers {
        scm('H/5 * * * *')
    }
    wrappers {
        nodejs('nodejs 16.18.0') // this is the name of the NodeJS installation in 
                         // Manage Jenkins -> Configure Tools -> NodeJS Installations -> Name
    }
    steps {
        dockerBuildAndPublish {
            repositoryName('registry.gitlab.com/nodejs136/nodejs-example-ci-cd')
            tag('${GIT_REVISION,length=9}')
            registryCredentials('gitlab')
            forcePull(false)
            forceTag(false)
            createFingerprints(false)
            skipDecorate()
        }
    }
}
