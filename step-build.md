# New Jobs
 - Name job as name folder in workspace
 - repo git
 - 
# Install plugin
- nodejs
- CloudBees Docker Build and Publish
- docker
# Integration 
$ cd jenkins-docker 

$ docker build -t docker-jenkins .

$ docker run -p 8080:8080 -p 50000:50000 -v /var/jenkins_home:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock --name jenkins -d --net host docker-jenkins

$ ls -ahl /var/run/docker.sock
$ chmod 777 /var/run/docker.sock

$ docker exec -it jenkins bash

# Jenkins Job DSL
- install plugin "job dsl"
- create new job
- checklis git
  - enter url https://gitlab.com/docker-project-list/jenkins.git
- select build step "Process Job DSLs"
  - enter repo jenkins script
    - add path jobs groovy "jenkins-script/job-dsl/nodejs.groovy"
- save
- manage jenkins 
    -   security
        - in process approval click approve

run command
docker exec -it jenkins bash
export PATH=$PATH:/var/jenkins_home/tools/jenkins.plugins.nodejs.tools.NodeJSInstallation/nodejs/bin
cd /var/jenkins_home/workspace/ && npm start

- Build and publish to registry
- create new job
- checklis git
  - enter url https://gitlab.com/docker-project-list/jenkins.git
- select build step "Process Job DSLs"
  - enter repo jenkins script
    - add path jobs groovy "jenkins-script/job-dsl/nodejsdocker.groovy"
- save
- manage jenkins 
    -   security
        - in process approval click approve

# Jenkins Pipeline
- add jenkinsfile to project nodejs-example-app > Jenkinsfile
- new job > nodejs docker pipeline > Pipeline >  ok
  - Pipeline > Definition > Pipeline script from SCM > SCM > Git > https://gitlab.com/nodejs136/nodejs-example-ci-cd.git
  - Script Path misc/Jenkinsfile
  - Save
  - Build Now
  - Console Log

- Build, Test, and run everything in docker containers- add jenkinsfile to project nodejs-example-app > Jenkinsfile
- add config tools for nodejs version
- new job > tests in docker > Pipeline >  ok
  - Pipeline > Definition > Pipeline script from SCM > SCM > Git > https://gitlab.com/nodejs136/nodejs-example-ci-cd.git
  - Script Path misc/Jenkinsfile.v2
  - Save
  - Build Now
  - Console Log


# Error 
- docker for class: groovy.lang.Binding
  Solution
  -  The issue was I needed to install the Docker Pipeline plugin in Jenkins.
  -  To install the plugin using the GUI:
  -  Dashboard > Manage Jenkins > Manage Plugins > Available (tab) > docker-workflow

-  no protocol: registry.gitlab.com/nodejs136/nodejs-example-ci-cd
  Solution
  - uncheck treat password on credential manager
- Error response from daemon: Head "https://registry-1.docker.io/v2/library/node/manifests/16.18.0": unauthorized: incorrect username or password
  Solution
  - 
  ```
      agent {
      docker {
        image                 'node:16-stretch'
        reuseNode             true
        registryUrl           'https://registry-1.docker.io'
        registryCredentialsId 'myRegistryCredentials'
      }
    }
  ```

# Jenkins Integrations
- Email integration
    - Dashboard > Manage Jenkins > Manage Plugins > Installed (tab) > Email Extension Plugin
    - Dashboard > Manage Jenkins > Configure System >  Extended E-mail Notification > enter server SMTP or mailtrap
    - new job > Email test > Pipeline >  ok
      - Poll SCM
        Schedule > H/5 * * * *
      - Pipeline > Definition > Pipeline script from SCM > SCM > Git > https://gitlab.com/docker-project-list/jenkins.git
      - Script Path jenkins-script/email-notifications/Jenkinsfile
      - Save
      - Build Now
      - Console Log



- Slask integration
    - Dashboard > Manage Jenkins > Manage Plugins > Installed (tab) > slack
    - open slack > app integration > webhook
    - copy https://hooks.slack.com/services/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 to 
    - new job > slack notification > Pipeline >  ok
      - Pipeline > Definition > Pipeline script from SCM > SCM > Git > https://gitlab.com/docker-project-list/jenkins.git
      - Script Path jenkins-script/slack-notifications/Jenkinsfile
      - Save
      - Build Now
      - Console Log

- Github integration 
    - Dashboard > Manage Jenkins > Manage Plugins > Installed (tab) > github branch source plugin
    - new job > my github repo > Github Organization >  ok
      - Pipeline > Definition > Pipeline script from SCM > SCM > Git > https://gitlab.com/docker-project-list/jenkins.git
      - owner => username github
      - Scan credentials
        - username
        - password |  Settings > Personal Access Token > generate token
      - docker exec -it jenkins bash
        - mkdir /var/jenkins_home/.gradle
        - chown 1000:1000 /var/jenkins_home/.gradle

        https://github.com/dikiharyadi19/gs-gradle

- Bitbucket integration
    - Dashboard > Manage Jenkins > Manage Plugins > Installed (tab) > bitbucket branch source plugin
    - new job > my github repo > Github Organization >  ok
      - Pipeline > Definition > Pipeline script from SCM > SCM > Git > https://gitlab.com/docker-project-list/jenkins.git
      - owner => username github
      - Scan credentials
        - username
        - password |  Settings > Personal Access Token > generate token

- Gitlab integration
    - Dashboard > Manage Jenkins > Manage Plugins > Installed (tab) > gitlab branch source plugin
    - new job > my github repo > Github Organization >  ok
      - Pipeline > Definition > Pipeline script from SCM > SCM > Git > https://gitlab.com/docker-project-list/jenkins.git
      - owner => username github
      - Scan credentials
        - username
        - password |  Settings > Personal Access Token > generate token

- JFrog Artifactory integration
    - Dashboard > Manage Plugins > Available (tab) > Artifactory  
    - login to https://jfrog.com/ > klik my server > create repository > gradle > finish
    - create group > deploy
    - create user > deploy and add user to group
    - create permission > deploy 
    - profile deploy> authentication setting > create api key 
    - Dashboard > Manage Jenkins > Configure system > Artifactory > 
      check Enable push to binary, server id enter .jfrog.io and URL with https://*.jfrog.io
      > use the credential plugin enter credential with password api key > tes connection
    - save

    - Dashboard > Manage Jenkins > global tool config > config grade, latest

    - new job > gradle publisher test > Pipeline >  ok
      - Pipeline > Definition > Pipeline script from SCM > SCM > Git > https://gitlab.com/docker-project-list/jenkins.git
      - Script Path jenkins-script/jfrog-integration/Jenkinsfile 
      - Save
      - Build Now
      - Console Log

- Custom API integration
    - Dashboard > Manage Jenkins > Manage Plugins > Available (tab) > http request
    - to bitbucket.com create oauth consumer andd add link jenkins to callback
    - register to credential in jenkins 
    - new job > gradle publisher test > Pipeline >  ok
      - Pipeline > Definition > Pipeline script from SCM > SCM > Git > https://gitlab.com/docker-project-list/jenkins.git
      - Script Path jenkins-script/custonapi-integration/Jenkinsfile 
      - Save
      - Build Now
      - Console Log

- Sonarqube integration with docker-compose
  - docker stop jenkins
  - cd /home/dik/docker/jenkins/jenkins-script/docker-compose 
  - docker-compose up -d
  - open sonarqube on port localhost:9002
  - Dashboard > Manage Jenkins > Manage Plugins > Available (tab) > sonarqube
  - Dashboard > Manage Jenkins > configuration tools and setup sonarqube
  - login to sonarqube administrator add generate tokens
  - add credential to jenkins
  - 

- Sonarqube integration within jenkins Pipeline
    - new job > sonarqube > Pipeline >  ok
      - Pipeline > Definition > Pipeline script from SCM > SCM > Git > https://gitlab.com/docker-project-list/jenkins.git
      - Script Path jenkins-script/sonarqube/Jenkinsfile 
      - Save
      - Build Now
      - Console Log

# Advanced Usage
- Jenkins Slave

- Jenkins Slave Using SSH

- Jenkins Slave jnlp

- Blue Ocean
    - Dashboard > Manage Plugins > Available (tab) > blue ocean


- ssh agent
  - Dashboard > Manage Plugins > Available (tab) > ssh agent

- security

- Authorization

- Authentication Providers

- OneLogin Integration with jenkins using SAML 
# references
https://jenkinsci.github.io/job-dsl-plugin/